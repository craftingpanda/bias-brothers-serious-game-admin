import App from './App';
import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';

test('When rendered, the welcome page should show.', () => {
    render(<App />);

    expect(screen.getByText('Welkom! Kies een onderdeel om te bewerken.')).toBeInTheDocument();
});