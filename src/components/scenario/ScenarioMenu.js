import { useState, useEffect, useCallback } from "react";
import ScenarioContent from "./ScenarioContent";
import ScenarioAdd from "./ScenarioAdd";
import ButtonList from "../ButtonList";
import ModalMenu from "../modal/ModalMenu";
import Backdrop from "../modal/Backdrop";
import SearchIcon from '@mui/icons-material/Search';

const ScenarioMenu = () => {
    const [scenarioList, setScenarioList] = useState([]);
    const [chosenIndex, setChosenIndex] = useState(0);
    const [scenarioContent, setScenarioContent] = useState([]);
    const [newScenario, addScenario] = useState(false);
    const [listForButtons, setListForButtons] = useState([]);
    const [openModalMenu, setOpenModalMenu] = useState(false);
    const [searchInput, setSearchInput] = useState('');

    const fetchAllScenarios = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/scenario');
        const data = await res.json();
        setScenarioList(data);
        setButtons(data);
        setChosenIndex(false);
        addScenario(false);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            await fetchAllScenarios();
        }
        fetchData();
    }, [fetchAllScenarios]);

    const handleScenarioClick = (clickedScenarioId) => {
        scenarioList.forEach(scenario => {
            if (scenario.id === clickedScenarioId) {
                addScenario(false);
                setScenarioContent(scenario);
                setChosenIndex(clickedScenarioId);
                setOpenModalMenu(false);
                setButtons(scenarioList);
            }
        });
    }

    const clickAddScenario = () => {
        setChosenIndex(false);
        addScenario(true);
    }

    const setButtons = (data) => {
        const buttonList = [];
        data.forEach(scenario => {
            const button = { id: scenario.id, name: scenario.title }
            buttonList.push(button);
        });
        setListForButtons(buttonList);
    }

    const cancelHandler = () => {
        setOpenModalMenu(false);
        setButtons(scenarioList);
    }

    const openMenu = () => {
        setOpenModalMenu(true);
    }

    const returnFilteredData = (searchValue) => {
        setSearchInput(searchValue);
        if (searchInput !== '') {
            const filteredData = scenarioList.filter((item) => {
                return Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase())
            })
            setButtons(filteredData);
        } else {
            setButtons(scenarioList);
        }
    }

    return (
        <div className="container-menu">
            <div className="btn-group">
                {scenarioList.length > 0 && <ButtonList list={listForButtons} handleItemClick={handleScenarioClick} color={chosenIndex} />}
                <button onClick={clickAddScenario} title="addScenario">+</button>
            </div>
            <div className="container-content">
                {!newScenario && !chosenIndex ? 'Selecteer een scenario of maak een nieuwe aan.' : ''}
                {chosenIndex && <ScenarioContent scenarioContent={scenarioContent} fetchAllScenarios={fetchAllScenarios} />}
                {newScenario ? <ScenarioAdd fetchAllScenarios={fetchAllScenarios} /> : ''}
            </div>
            <button className="button-menu" onClick={openMenu} title="menu"><SearchIcon /></button>
            <div>
                <button className="button-add" onClick={clickAddScenario}>+</button>
            </div>
            <div>
                {openModalMenu && <ModalMenu modalQuestion="Maak een keuze uit het menu" cancelHandler={cancelHandler} modalCancelText="Annuleren" handleClick={handleScenarioClick} listForButtons={listForButtons} listForResults={(e) => returnFilteredData(e.target.value)} />}
                {openModalMenu && <Backdrop cancelModal={cancelHandler} />}
            </div>
        </div >
    );
}

export default ScenarioMenu;
