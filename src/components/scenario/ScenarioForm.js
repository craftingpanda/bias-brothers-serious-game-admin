import { useState, useEffect, useCallback } from "react";

const ScenarioForm = ({ postOrPut,
    title, setTitle,
    text, setText,
    measureQuestion1, measureQuestion2, measureQuestion3,
    setMeasureQuestion1, setMeasureQuestion2, setMeasureQuestion3,
    biasQuestion1, biasQuestion2, biasQuestion3,
    setBiasQuestion1, setBiasQuestion2, setBiasQuestion3,
}) => {
    const [measureList, setMeasureList] = useState([]);
    const [biasList, setBiasList] = useState([]);


    const fetchMeasures = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/measure');
        const data = await res.json();
        setMeasureList(data);
    }, []);

    const fetchBiases = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/bias');
        const data = await res.json();
        setBiasList(data);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            await fetchMeasures();
            await fetchBiases();
        }
        fetchData();
    }, [fetchMeasures, fetchBiases]);

    const handleMeasureDropdownSelect = (e, index) => {
        measureList.forEach(measure => {
            if (index === 1) {
                if (e.target.value === measure.answer) {
                    setMeasureQuestion1({ id: measureQuestion1.id, points: measureQuestion1.points, measure: {id: measure.id, answer: e.target.value }});
                }
            } else if (index === 2) {
                if (e.target.value === measure.answer) {
                    setMeasureQuestion2({ id: measureQuestion2.id, points: measureQuestion1.points, measure: {id: measure.id, answer: e.target.value } });
                }
            } else if (index === 3) {
                if (e.target.value === measure.answer) {
                    setMeasureQuestion3({ id: measureQuestion3.id, points: measureQuestion1.points, measure: {id: measure.id, answer: e.target.value } });
                }
            }
        });
    }

    const handleBiasDropdownSelect = (e, index) => {
        biasList.forEach(bias => {
            if (index === 1) {
                if (e.target.value === bias.name) {
                    setBiasQuestion1({ id: biasQuestion1.id, points: biasQuestion1.points, bias: {id: bias.id, name: e.target.value, description: bias.description, example: bias.example}});
                }
            } else if (index === 2) {
                if (e.target.value === bias.name) {
                    setBiasQuestion2({ id: biasQuestion2.id, points: biasQuestion2.points, bias: {id: bias.id, name: e.target.value, description: bias.description, example: bias.example }});
                }
            } else if (index === 3) {
                if (e.target.value === bias.name) {
                    setBiasQuestion3({ id: biasQuestion3.id, points: biasQuestion3.points, bias: {id: bias.id, name: e.target.value, description: bias.description, example: bias.example }});
                }
            }
        });
    }

    return (
        <>
            <form className="scenarioContent-form" onSubmit={postOrPut}>
                <label id="scenarioTitel">Scenario titel</label>
                <textarea value={title} onChange={e => setTitle(e.target.value)} />
                <label>Scenario:</label>
                <textarea className="textarea-big" value={text} onChange={e => setText(e.target.value)} />
                <div className="textareas-answers">
                    <div className="textarea-rows">
                        <label>Maatregel 1</label>
                        <select onChange={e => handleMeasureDropdownSelect(e, 1)}>
                            <option defaultValue="Kies een andere maatregel">Kies een andere maatregel</option>
                            {measureList.map((measure) => <option key={measure.id} value={measure.answer}>{measure.answer}</option>)}
                        </select>
                        <label>Punten</label>
                    </div>
                    <div className="textarea-rows textarea-rows2">
                        <textarea className="textarea-med" value={measureQuestion1.measure.answer} onChange={e => setMeasureQuestion1({ ...measureQuestion1, answer: e.target.value })} />
                        <textarea value={measureQuestion1.points} onChange={e => setMeasureQuestion1({ ...measureQuestion1, points: e.target.value })} />
                    </div>
                    <div className="textarea-rows">
                        <label>Maatregel 2</label>
                        <select onChange={e => handleMeasureDropdownSelect(e, 2)}>
                            <option defaultValue="Kies een andere maatregel">Kies een andere maatregel</option>
                            {measureList.map((measure) => <option key={measure.id} value={measure.answer}>{measure.answer}</option>)}
                        </select>
                        <label>Punten</label>
                    </div>
                    <div className="textarea-rows textarea-rows2">
                        <textarea className="textarea-med" value={measureQuestion2.measure.answer} onChange={e => setMeasureQuestion2({ ...measureQuestion2, measure: {answer: e.target.value } })} readOnly />
                        <textarea value={measureQuestion2.points} onChange={e => setMeasureQuestion2({ ...measureQuestion2, points: e.target.value })} />
                    </div>
                    <div className="textarea-rows">
                        <label>Maatregel 3</label>
                        <select onChange={e => handleMeasureDropdownSelect(e, 3)}>
                            <option defaultValue="Kies een andere maatregel">Kies een andere maatregel</option>
                            {measureList.map((measure) => <option key={measure.id} value={measure.answer}>{measure.answer}</option>)}
                        </select>
                        <label>Punten</label>
                    </div>
                    <div className="textarea-rows textarea-rows2">
                        <textarea className="textarea-med" value={measureQuestion3.measure.answer} onChange={e => setMeasureQuestion3({ ...measureQuestion3, measure: {answer: e.target.value } })} />
                        <textarea value={measureQuestion3.points} onChange={e => setMeasureQuestion3({ ...measureQuestion3, points: e.target.value })} />
                    </div>

                    <div className="textareas-answers">
                        <div className="textarea-rows">
                            <label>Bias 1</label>
                            <select onChange={e => handleBiasDropdownSelect(e, 1)}>
                                <option defaultValue="Kies een andere bias">Kies een andere bias</option>
                                {biasList.map((bias) => <option key={bias.id} value={bias.name}>{bias.name}</option>)}
                            </select>
                            <label>Punten</label>
                        </div>
                        <div className="textarea-rows textarea-rows2">
                            <textarea className="textarea-small" value={biasQuestion1.bias.name} onChange={e => setBiasQuestion1({ ...biasQuestion1, bias: {name: e.target.value }})} />
                            <textarea value={biasQuestion1.points} onChange={e => setBiasQuestion1({ ...biasQuestion1, points: e.target.value })} />
                        </div>

                        <div className="textarea-rows">
                            <label>Bias 2</label>
                            <select onChange={e => handleBiasDropdownSelect(e, 2)}>
                                <option defaultValue="Kies een andere bias">Kies een andere bias</option>
                                {biasList.map((bias) => <option key={bias.id} value={bias.name}>{bias.name}</option>)}
                            </select>
                            <label>Punten</label>
                        </div>
                        <div className="textarea-rows textarea-rows2">
                            <textarea className="textarea-small" value={biasQuestion2.bias.name} onChange={e => setBiasQuestion2({ ...biasQuestion2, bias: {name: e.target.value }})} />
                            <textarea value={biasQuestion2.points} onChange={e => setBiasQuestion2({ ...biasQuestion2, points: e.target.value })} />
                        </div>

                        <div className="textarea-rows">
                            <label>Bias 3</label>
                            <select onChange={e => handleBiasDropdownSelect(e, 3)}>
                                <option defaultValue="Kies een andere bias">Kies een andere bias</option>
                                {biasList.map((bias) => <option key={bias.id} value={bias.name}>{bias.name}</option>)}
                            </select>
                            <label>Punten</label>
                        </div>
                        <div className="textarea-rows textarea-rows2">
                            <textarea className="textarea-small" value={biasQuestion3.bias.name} onChange={e => setBiasQuestion3({ ...biasQuestion3, bias: {name: e.target.value }})} />
                            <textarea value={biasQuestion3.points} onChange={e => setBiasQuestion3({ ...biasQuestion3, points: e.target.value })} />
                        </div>
                </div>
                </div>

            </form>
        </>
    );
}

export default ScenarioForm;
