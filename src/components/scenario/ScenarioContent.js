import { useState, useEffect } from "react"
import Backdrop from "../modal/Backdrop";
import ScenarioForm from "./ScenarioForm";
import checkModal from "../../Functions/checkModal";

const ScenarioContent = ({ scenarioContent, fetchAllScenarios }) => {
    const [title, setTitle] = useState('');
    const [text, setText] = useState('');
    const [measureQuestion1, setMeasureQuestion1] = useState({
        id: '',
        points: '',
        measure: {
            id: '',
            answer: ''
        }
    });
    const [measureQuestion2, setMeasureQuestion2] = useState({
        id: '',
        points: '',
        measure: {
            id: '',
            answer: ''
        }
    });
    const [measureQuestion3, setMeasureQuestion3] = useState({
        id: '',
        points: '',
        measure: {
            id: '',
            answer: ''
        }
    });
    const [biasQuestion1, setBiasQuestion1] = useState({
        id: '',
        points: '',
        bias:{
        }
    });
    const [biasQuestion2, setBiasQuestion2] = useState({
        id: '',
        points: '',
        bias:{
        }
    });
    const [biasQuestion3, setBiasQuestion3] = useState({
        id: '',
        points: '',
        bias:{
        }
    });


    const [modalIsOpen, setModalIsOpen] = useState(null);
    const [modalSaveOpen, setModalSaveOpen] = useState(null);

    useEffect(() => {
        setTitle(scenarioContent.title);
        setText(scenarioContent.text);
        setMeasureQuestion1({ id: scenarioContent.measureQuestions[0].id, points: scenarioContent.measureQuestions[0].points, measure: {id: scenarioContent.measureQuestions[0].measure.id, answer: scenarioContent.measureQuestions[0].measure.answer}});
        setMeasureQuestion2({ id: scenarioContent.measureQuestions[1].id, points: scenarioContent.measureQuestions[1].points, measure: {id: scenarioContent.measureQuestions[1].measure.id, answer: scenarioContent.measureQuestions[1].measure.answer}});
        setMeasureQuestion3({ id: scenarioContent.measureQuestions[2].id, points: scenarioContent.measureQuestions[2].points, measure: {id: scenarioContent.measureQuestions[2].measure.id, answer: scenarioContent.measureQuestions[2].measure.answer}});
        setBiasQuestion1({id: scenarioContent.biasQuestions[0].id, points: scenarioContent.biasQuestions[0].points, bias: {id: scenarioContent.biasQuestions[0].bias.id, name: scenarioContent.biasQuestions[0].bias.name, description: scenarioContent.biasQuestions[0].bias.description, example: scenarioContent.biasQuestions[0].bias.example}})
        setBiasQuestion2({id: scenarioContent.biasQuestions[1].id, points: scenarioContent.biasQuestions[1].points, bias: {id: scenarioContent.biasQuestions[1].bias.id, name: scenarioContent.biasQuestions[1].bias.name, description: scenarioContent.biasQuestions[1].bias.description, example: scenarioContent.biasQuestions[1].bias.example}})
        setBiasQuestion3({id: scenarioContent.biasQuestions[2].id, points: scenarioContent.biasQuestions[2].points, bias: {id: scenarioContent.biasQuestions[2].bias.id, name: scenarioContent.biasQuestions[2].bias.name, description: scenarioContent.biasQuestions[2].bias.description, example: scenarioContent.biasQuestions[2].bias.example}})
    }, [scenarioContent]);

    const putUpdatedScenario = async (e) => {
        e.preventDefault();
        const updatedScenario = {
            id: scenarioContent.id,
            measureQuestions: [measureQuestion1, measureQuestion2, measureQuestion3],
            biasQuestions: [biasQuestion1, biasQuestion2, biasQuestion3],
            title: title,
            text: text
        };
        setModalSaveOpen(true);
        if (modalSaveOpen) {
            await putRequest(updatedScenario);
        }
    }

    const putRequest = async (updatedScenario) => {
        await fetch(`http://localhost:8080/api/scenario/update/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedScenario)
        });
        fetchAllScenarios(); // Refreshes the list and loads the buttons
    }

    const deleteScenario = async () => {
        await fetch(`http://localhost:8080/api/scenario/delete/${scenarioContent.id}`, {
            method: 'DELETE'
        });
        fetchAllScenarios();
    }

    const closeModalHandler = () => {
        setModalIsOpen(false);
        setModalSaveOpen(false);
    }

    const openModalHandler = () => {
        setModalIsOpen(true);
    }

    return (
        <div className="div-content">
            <ScenarioForm
                title={title} setTitle={setTitle}
                text={text} setText={setText}
                measureQuestion1={measureQuestion1} setMeasureQuestion1={setMeasureQuestion1}
                measureQuestion2={measureQuestion2} setMeasureQuestion2={setMeasureQuestion2}
                measureQuestion3={measureQuestion3} setMeasureQuestion3={setMeasureQuestion3}
                biasQuestion1={biasQuestion1} setBiasQuestion1={setBiasQuestion1}
                biasQuestion2={biasQuestion2} setBiasQuestion2={setBiasQuestion2}
                biasQuestion3={biasQuestion3} setBiasQuestion3={setBiasQuestion3}
                openModalHandler={openModalHandler}
                greyedOut={true}
            />
            <div className="div-save-button">
                <input className="button-save" onClick={putUpdatedScenario} type="submit" value="Opslaan" />
                <button className="button-delete" onClick={openModalHandler}>Verwijder Scenario</button>

            </div>
            {(modalIsOpen || modalSaveOpen) && <Backdrop cancelModal={closeModalHandler} />}
            {checkModal(modalIsOpen, modalSaveOpen, closeModalHandler, deleteScenario, putUpdatedScenario)}
        </div>
    );
}

export default ScenarioContent;
