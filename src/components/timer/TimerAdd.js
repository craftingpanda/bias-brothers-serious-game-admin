import { useState } from "react";
import Modal from "../modal/Modal";
import Backdrop from "../modal/Backdrop";

const TimerAdd = ({ fetchAllTimers }) => {
    const [name, setName] = useState('');
    const [alertModal, setAlertModal] = useState(null);
    const [emptyFieldModal, setEmptyFieldModal] = useState(null);

    const postTimer = async (e) => {
        e.preventDefault();
        if (name === '') {
            setEmptyFieldModal(true);
            return;
        }
        const timer = {
            name: name
        };
        setAlertModal(true);
        if (alertModal) {
            await postRequest(timer);
        }

    }

    const postRequest = async (timer) => {
        await fetch('http://localhost:8080/api/timer/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(timer)
        });
        fetchAllTimers();
    }

    const closeModalHandler = () => {
        setAlertModal(false);
        setEmptyFieldModal(false);
    }

    return (
        <>
            <form className="timerContent-form" onSubmit={postTimer}>
                <label id="name">Aantal minuten: </label>
                <textarea value={name} onChange={e => setName(e.target.value)} />

                <div className="div-save-button">
                    <div/>
                    <div/>
                    <input className="button-save" type="submit" value="Opslaan" />
                </div>
                <div>
                    {/* Opens the modal for the save button. */}
                    {alertModal && <Modal cancelHandler={closeModalHandler} confirmHandler={postTimer} modalQuestion='Weet je zeker dat je een nieuwe timer wilt toevoegen?' modalCancelText='Annuleren' modalConfirmText='Opslaan' />}
                    {alertModal && <Backdrop cancelModal={closeModalHandler} />}

                    {/* Opens the modal when there are empty fields */}
                    {emptyFieldModal && <Modal cancelHandler={closeModalHandler} confirmHandler={closeModalHandler} modalQuestion='Vul alle velden in voordat je gaat opslaan!' modalCancelText='Sluiten' modalConfirmText='Velden invullen' />}
                    {emptyFieldModal && <Backdrop cancelModal={closeModalHandler} />}
                </div>
            </form>
        </>
    );
}

export default TimerAdd;