import { useState } from "react";
import Modal from "../modal/Modal";
import Backdrop from "../modal/Backdrop";

const NewsArticleAdd = ({ fetchAllNewsArticles }) => {
    const [title, setTitle] = useState('');
    const [message, setMessage] = useState('');
    const [source, setSource] = useState('');
    const [popUp, setPopup] = useState(false);
    const [alertModal, setAlertModal] = useState(null);
    const [emptyFieldModal, setEmptyFieldModal] = useState(null);

    const postNewsArticle = async (e) => {
        e.preventDefault();
        if (title === '' || message === '' || source === '') {
            setEmptyFieldModal(true);
            return;
        }
        const newsArticle = {
            title: title,
            message: message,
            source: source,
            popUp: popUp
        };
        setAlertModal(true);
        if (alertModal) {
            await postRequest(newsArticle);
        }
    }

    const postRequest = async (newsArticle) => {
        await fetch('http://localhost:8080/api/news_article/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(newsArticle)
        })
        fetchAllNewsArticles();
    }

    const closeModalHandler = () => {
        setAlertModal(false);
        setEmptyFieldModal(false);
    }

    return (
        <>
            <form className="newsArticleContent-form" onSubmit={postNewsArticle}>
                <label id="newsArticleNaam">Nieuwsartikel titel</label>
                <textarea value={title} onChange={e => setTitle(e.target.value)} />
                <label>Nieuwsartikel inhoud</label>
                <textarea value={message} onChange={e => setMessage(e.target.value)} />
                <label>Nieuwsartikel bron</label>
                <textarea value={source} onChange={e => setSource(e.target.value)} />
                <label>Nieuwsartikel pop-up</label>
                <input type="checkbox" value={popUp} onChange={e => setPopup(e.target.checked)} checked={popUp} />
                <div className="div-save-button">
                    <input className="button-save" type="submit" value="Opslaan" />
                </div>
                <div>
                    {/* Opens the modal for the save button. */}
                    {alertModal && <Modal cancelHandler={closeModalHandler} confirmHandler={postNewsArticle} modalQuestion='Weet je zeker dat je een nieuw nieuwsbericht wilt toevoegen?' modalCancelText='Annuleren' modalConfirmText='Opslaan' />}
                    {alertModal && <Backdrop cancelModal={closeModalHandler} />}
                    {/* Opens the modal when there are empty fields */}
                    {emptyFieldModal && <Modal cancelHandler={closeModalHandler} confirmHandler={closeModalHandler} modalQuestion='Vul alle velden in voordat je gaat opslaan!' modalCancelText='Sluiten' modalConfirmText='Velden invullen' />}
                    {emptyFieldModal && <Backdrop cancelModal={closeModalHandler} />}
                </div>
            </form>
        </>
    );
}

export default NewsArticleAdd;
