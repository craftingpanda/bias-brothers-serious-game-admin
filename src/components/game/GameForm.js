const GameForm = ({ game, setGame, roundDropdown, handleRoundDropdown }) => {
    return (
        <form className="gameContent-form">
            <label>Game titel: </label>
            <textarea onChange={e => setGame({...game, name: e.target.value})} value={game.name}/>
            <label>Ronde 1</label>
            <select onChange={e => handleRoundDropdown(e, 1)}>
                <option defaultValue="Kies een ronde">Kies een ronde</option>
                {roundDropdown.map(round => <option key={round.id} value={round.roundNumber + " " + round.title}>{round.roundNumber + " " + round.title}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={game.rounds[0].roundNumber + " " + game.rounds[0].title}/>
            <label>Ronde 2</label>
            <select onChange={e => handleRoundDropdown(e, 2)}>
                <option defaultValue="Kies een ronde">Kies een ronde</option>
                {roundDropdown.map(round => <option key={round.id} value={round.roundNumber + " " + round.title}>{round.roundNumber + " " + round.title}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={game.rounds[1].roundNumber + " " + game.rounds[1].title}/>
            <label>Ronde 3</label>
            <select onChange={e => handleRoundDropdown(e, 3)}>
                <option defaultValue="Kies een ronde">Kies een ronde</option>
                {roundDropdown.map(round => <option key={round.id} value={round.roundNumber + " " + round.title}>{round.roundNumber + " " + round.title}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={game.rounds[2].roundNumber + " " + game.rounds[2].title}/>
            <label>Ronde 4</label>
            <select onChange={e => handleRoundDropdown(e, 4)}>
                <option defaultValue="Kies een ronde">Kies een ronde</option>
                {roundDropdown.map(round => <option key={round.id} value={round.roundNumber + " " + round.title}>{round.roundNumber + " " + round.title}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={game.rounds[3].roundNumber + " " + game.rounds[3].title}/>
            <label>Ronde 5</label>
            <select onChange={e => handleRoundDropdown(e, 5)}>
                <option defaultValue="Kies een ronde">Kies een ronde</option>
                {roundDropdown.map(round => <option key={round.id} value={round.roundNumber + " " + round.title}>{round.roundNumber + " " + round.title}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={game.rounds[4].roundNumber + " " + game.rounds[4].title}/>
            <label>Ronde 6</label>
            <select onChange={e => handleRoundDropdown(e, 6)}>
                <option defaultValue="Kies een ronde">Kies een ronde</option>
                {roundDropdown.map(round => <option key={round.id} value={round.roundNumber + " " + round.title}>{round.roundNumber + " " + round.title}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={game.rounds[5].roundNumber + " " + game.rounds[5].title}/>
        </form>
    );
};

export default GameForm;