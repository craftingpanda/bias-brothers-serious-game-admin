import { useState, useEffect } from "react";
import GameForm from "./GameForm";
import Backdrop from "../modal/Backdrop";
import Modal from "../modal/Modal";

const GameAdd = ({ fetchAllGames}) => {
    const [game, setGame] = useState({
            name: "",
            rounds: [{
                        id: 7,
                        title: "Lege ronde",
                        roundNumber: "",
                        canvases: [

                        ],
                        biasQuestions: [

                        ],
                        scenario: {

                        },
                        timer: {

                        }
                    },
                {
                    id: 7,
                    title: "Lege ronde",
                    roundNumber: "",
                    canvases: [

                    ],
                    biasQuestions: [

                    ],
                    scenario: {

                    },
                    timer: {

                    }
                },
                {
                    id: 7,
                    title: "Lege ronde",
                    roundNumber: "",
                    canvases: [

                    ],
                    biasQuestions: [

                    ],
                    scenario: {

                    },
                    timer: {

                    }
                },
                {
                    id: 7,
                    title: "Lege ronde",
                    roundNumber: "",
                    canvases: [

                    ],
                    biasQuestions: [

                    ],
                    scenario: {

                    },
                    timer: {

                    }
                },
                {
                    id: 7,
                    title: "Lege ronde",
                    roundNumber: "",
                    canvases: [

                    ],
                    biasQuestions: [

                    ],
                    scenario: {

                    },
                    timer: {

                    }
                },
                {
                    id: 7,
                    title: "Lege ronde",
                    roundNumber: "",
                    canvases: [

                    ],
                    biasQuestions: [

                    ],
                    scenario: {

                    },
                    timer: {

                    }
                }
                    ]});
    let [roundDropdown, setRoundDropdown] = useState([]);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [alertModal, setAlertModal] = useState(null);
    const [emptyFieldModal, setEmptyFieldModal] = useState(null);

    useEffect(async () => {
        await fetchAllRounds();
    }, []);

    const postGame = async (e) => {
        e.preventDefault();
        if (game.name === '') {
            setEmptyFieldModal(true);
            return;
        }
        setAlertModal(true);
        if (alertModal) {
            await postRequest();
        }
    }

    const postRequest = async () => {
        await fetch('http://localhost:8080/api/game/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(game)
        })
        fetchAllGames();
    }

    const fetchAllRounds = async () => {
        const res = await fetch('http://localhost:8080/api/round');
        const data = await res.json();
        setRoundDropdown(data);
    }

    const handleRoundDropdown = (e, index) => {
        const updatedRounds = game.rounds;
        roundDropdown.forEach(round => {
            if (index === 1) {
                if (e.target.value === round.roundNumber + " " + round.title) {
                    updatedRounds[0] = round;
                    setGame({ ...game, rounds: updatedRounds });
                }
            } else if (index === 2) {
                if (e.target.value === round.roundNumber + " " + round.title) {
                    updatedRounds[1] = round;
                    setGame({ ...game, rounds: updatedRounds });
                }
            } else if (index === 3) {
                if (e.target.value === round.roundNumber + " " + round.title) {
                    updatedRounds[2] = round;
                    setGame({ ...game, rounds: updatedRounds });
                }
            } else if (index === 4) {
                if (e.target.value === round.roundNumber + " " + round.title) {
                    updatedRounds[3] = round;
                    setGame({ ...game, rounds: updatedRounds });
                }
            } else if (index === 5) {
                if (e.target.value === round.roundNumber + " " + round.title) {
                    updatedRounds[4] = round;
                    setGame({ ...game, rounds: updatedRounds });
                }
            } else if (index === 6) {
                if (e.target.value === round.roundNumber + " " + round.title) {
                    updatedRounds[5] = round;
                    setGame({ ...game, rounds: updatedRounds });
                }
            }
        });
    }

    const openModalHandler = () => {
        setModalIsOpen(true);
    }

    const closeModalHandler = () => {
        setAlertModal(false);
        setEmptyFieldModal(false);
    }

    return (
        <div className="div-content">
            <GameForm game={game} setGame={setGame} roundDropdown={roundDropdown}
                       handleRoundDropdown={handleRoundDropdown} openModalHandler={openModalHandler} />
            <div className="div-save-button">
                <input className="button-save" onClick={postGame} type="submit" value="Opslaan" />
            </div>
            {/* Opens the modal for the save button. */}
            {alertModal && <Modal cancelHandler={closeModalHandler} confirmHandler={postGame} modalQuestion='Weet je zeker dat je een nieuwe game wilt toevoegen?' modalCancelText='Annuleren' modalConfirmText='Opslaan' />}
            {alertModal && <Backdrop cancelModal={closeModalHandler} />}
            {/* Opens the modal when there are empty fields */}
            {emptyFieldModal && <Modal cancelHandler={closeModalHandler} confirmHandler={closeModalHandler} modalQuestion='Vul alle velden in voordat je gaat opslaan!' modalCancelText='Sluiten' modalConfirmText='Velden invullen' />}
            {emptyFieldModal && <Backdrop cancelModal={closeModalHandler} />}

        </div>
    );
};

export default GameAdd;
