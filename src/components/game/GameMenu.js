import { useState, useEffect, useCallback } from "react";
import ButtonList from "../ButtonList";
import GameContent from "../game/GameContent";
import GameAdd from "../game/GameAdd";
import SearchIcon from "@mui/icons-material/Search";
import ModalMenu from "../modal/ModalMenu";
import Backdrop from "../modal/Backdrop";

const GameMenu = () => {
    const [gameList, setGameList] = useState([]);
    const [gameContent, setGameContent] = useState([]);
    const [chosenIndex, setChosenIndex] = useState(false);
    const [newGame, addGame] = useState(false);
    const [listForButtons, setListForButtons] = useState([]);
    const [openModalMenu, setOpenModalMenu] = useState(false);
    const [searchInput, setSearchInput] = useState('');

    const fetchAllGames = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/game');
        const data = await res.json();
        setGameList(data);
        setListForButtons(data);
        setChosenIndex(false);
        addGame(false);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            await fetchAllGames();
        }
        fetchData();
    }, [fetchAllGames]);

    const clickAddGame = () => {
        setChosenIndex(false);
        addGame(true);
    }

    const handleGameClick = (clickedGameId) => {
        gameList.forEach(game => {
            if (game.id === clickedGameId) {
                addGame(false);
                setGameContent(game);
                setChosenIndex(clickedGameId);
                setOpenModalMenu(false);
                setListForButtons(gameList);
            }
        });
    }

    const cancelHandler = () => {
        setOpenModalMenu(false);
        setListForButtons(gameList);
    }

    const openMenu = () => {
        setOpenModalMenu(true);
    }

    const returnSearchList = (searchValue) => {
        setSearchInput(searchValue);
        if (searchInput !== '') {
            const filteredData = gameList.filter((item) => {
                return Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase())
            })
            setListForButtons(filteredData);
        } else {
            setListForButtons(gameList);
        }
    }

    return (
        <div className="container-menu">
            <div className="btn-group">
                {gameList.length > 0 && <ButtonList list={listForButtons} handleItemClick={handleGameClick} color={chosenIndex} />}
                <button onClick={clickAddGame} title="addBias">+</button>
            </div>
            <div className="container-content">
                {!newGame && !chosenIndex ? 'Selecteer een game of maak een nieuwe aan.' : ''}
                {chosenIndex && <GameContent gameContent={gameContent} fetchAllGames={fetchAllGames} />}
                {newGame ? <GameAdd fetchAllGames={fetchAllGames} /> : ''}
            </div>
            <button className="button-menu" onClick={openMenu} title="menu"><SearchIcon /></button>
            <div>
                <button className="button-add" onClick={clickAddGame}>+</button>
            </div>
            <div>
                {openModalMenu && <ModalMenu modalQuestion="Maak een keuze uit het menu" cancelHandler={cancelHandler} modalCancelText="Annuleren" handleClick={handleGameClick} listForButtons={listForButtons} listForResults={(e) => returnSearchList(e.target.value)} />}
                {openModalMenu && <Backdrop cancelModal={cancelHandler} />}
            </div>
        </div>
    );
};

export default GameMenu;