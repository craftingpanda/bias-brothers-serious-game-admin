import { useState, useEffect } from "react";
import Backdrop from "../modal/Backdrop";
import checkModal from "../../Functions/checkModal";
import GameForm from "./GameForm";

const GameContent = ({ gameContent, fetchAllGames }) => {
    const [roundDropdown, setRoundDropdown] = useState([]);
    const [updatedGame, setUpdatedGame] = useState(null);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [modalSaveOpen, setModalSaveOpen] = useState(false);

    useEffect(async () => {
        await fetchAllRounds();
        setUpdatedGame(gameContent);
    }, [gameContent]);

    const fetchAllRounds = async () => {
        const res = await fetch('http://localhost:8080/api/round');
        const data = await res.json();
        setRoundDropdown(data);
    }

    const deleteGame = async () => {
        await fetch(`http://localhost:8080/api/game/delete/${gameContent.id}`, {
            method: 'DELETE'
        });
        fetchAllGames();
    }

    const putUpdatedGame = async (e) => {
        e.preventDefault();
        setModalSaveOpen(true);
        if (modalSaveOpen) {
            await putRequest();
        }
    }

    const putRequest = async () => {
        await fetch(`http://localhost:8080/api/game/update/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedGame)
        });
        fetchAllGames();
    }

    const handleRoundDropdown = (e, index) => {
        const updatedRounds = updatedGame.rounds;
        roundDropdown.forEach(round => {
            if (index === 1) {
                if (e.target.value === round.roundNumber + " " + round.title) {
                    updatedRounds[0] = round;
                    setUpdatedGame({ ...updatedGame, rounds: updatedRounds });
                }
            } else if (index === 2) {
                if (e.target.value === round.roundNumber + " " + round.title) {
                    updatedRounds[1] = round;
                    setUpdatedGame({ ...updatedGame, rounds: updatedRounds });
                }
            } else if (index === 3) {
                if (e.target.value === round.roundNumber + " " + round.title) {
                    updatedRounds[2] = round;
                    setUpdatedGame({ ...updatedGame, rounds: updatedRounds });
                }
            } else if (index === 4) {
                if (e.target.value === round.roundNumber + " " + round.title) {
                    updatedRounds[3] = round;
                    setUpdatedGame({ ...updatedGame, rounds: updatedRounds });
                }
            } else if (index === 5) {
                if (e.target.value === round.roundNumber + " " + round.title) {
                    updatedRounds[4] = round;
                    setUpdatedGame({ ...updatedGame, rounds: updatedRounds });
                }
            } else if (index === 6) {
                if (e.target.value === round.roundNumber + " " + round.title) {
                    updatedRounds[5] = round;
                    setUpdatedGame({ ...updatedGame, rounds: updatedRounds });
                }
            }
        });
    }

    const closeModalHandler = () => {
        setModalIsOpen(false);
        setModalSaveOpen(false);
    }

    const openModalHandler = () => {
        setModalIsOpen(true);
    }

    return (
        <div className="div-content">
            {updatedGame !== null && (
                <GameForm game={updatedGame} setGame={setUpdatedGame} roundDropdown={roundDropdown}
                           handleRoundDropdown={handleRoundDropdown} openModalHandler={openModalHandler} />
            )}
            <div className="div-save-button">
                <input className="button-save" onClick={putUpdatedGame} type="submit" value="Opslaan" />
                <button className="button-delete" onClick={openModalHandler}>Verwijder ronde</button>
            </div>

            {(modalIsOpen || modalSaveOpen) && <Backdrop cancelModal={closeModalHandler} />}
            {checkModal(modalIsOpen, modalSaveOpen, closeModalHandler, deleteGame, putUpdatedGame)}
            <div style={{marginTop: "20px"}} className="div-save-button"/>
        </div>
    );
};

export default GameContent;