import { useState } from "react";
import Modal from "../modal/Modal";
import Backdrop from "../modal/Backdrop";

const MetricsAdd = ({ fetchAllMetrics }) => {
    const [name, setName] = useState('');
    const [alertModal, setAlertModal] = useState(null);
    const [emptyFieldModal, setEmptyFieldModal] = useState(null);

    const postMetric = async (e) => {
        e.preventDefault();
        if (name === '') {
            setEmptyFieldModal(true);
            return;
        }
        const metric = {
            name: name
        };
        setAlertModal(true);
        if (alertModal) {
            await postRequest(metric);
        }

    }

    const postRequest = async (metric) => {
        await fetch('http://localhost:8080/api/metric/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(metric)
        });
        fetchAllMetrics();
    }

    const closeModalHandler = () => {
        setAlertModal(false);
        setEmptyFieldModal(false);
    }

    return (
        <>
            <form className="metricContent-form" onSubmit={postMetric}>
                <label id="metricNaam">Metric naam: </label>
                <textarea value={name} onChange={e => setName(e.target.value)} />
                <div className="div-save-button">
                    <input className="button-save" type="submit" value="Opslaan" />
                </div>
                <div>
                    {/* Opens the modal for the save button. */}
                    {alertModal && <Modal cancelHandler={closeModalHandler} confirmHandler={postMetric} modalQuestion='Weet je zeker dat je een nieuwe metriek wilt toevoegen?' modalCancelText='Annuleren' modalConfirmText='Opslaan' />}
                    {alertModal && <Backdrop cancelModal={closeModalHandler} />}
                    {/* Opens the modal when there are empty fields */}
                    {emptyFieldModal && <Modal cancelHandler={closeModalHandler} confirmHandler={closeModalHandler} modalQuestion='Vul alle velden in voordat je gaat opslaan!' modalCancelText='Sluiten' modalConfirmText='Velden invullen' />}
                    {emptyFieldModal && <Backdrop cancelModal={closeModalHandler} />}
                </div>
            </form>
        </>
    );
}

export default MetricsAdd;