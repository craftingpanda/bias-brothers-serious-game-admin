import { useCallback, useEffect, useState } from "react";
import MetricsAdd from "./MetricsAdd";
import ButtonList from "../ButtonList";
import MetricsContent from "./MetricsContent";
import ModalMenu from "../modal/ModalMenu";
import Backdrop from "../modal/Backdrop";
import SearchIcon from '@mui/icons-material/Search';

const MetricsMenu = () => {
    const [metricsList, setMetricsList] = useState([]);
    const [metricContent, setMetricContent] = useState([]);
    const [chosenIndex, setChosenIndex] = useState(0);
    const [newMetric, addMetric] = useState(false);
    const [listForButtons, setListForButtons] = useState([]);
    const [openModalMenu, setOpenModalMenu] = useState(false);
    const [searchInput, setSearchInput] = useState('');

    const fetchAllMetrics = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/metric');
        const data = await res.json();
        setMetricsList(data);
        setButtons(data);
        setChosenIndex(false);
        addMetric(false);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            fetchAllMetrics();
        }
        fetchData();
    }, [fetchAllMetrics]);

    const handleMetricClick = (clickedMetricId) => {
        metricsList.forEach(metric => {
            if (metric.id === clickedMetricId) {
                addMetric(false);
                setMetricContent(metric);
                setChosenIndex(clickedMetricId);
                setOpenModalMenu(false);
                setSearchInput('');
            }
        });
    }

    const clickAddMetric = () => {
        setChosenIndex(false);
        addMetric(true)
    }

    const setButtons = (data) => {
        const buttonList = [];
        data.forEach(metric => {
            const button = { id: metric.id, name: metric.name }
            buttonList.push(button);
        });
        setListForButtons(buttonList);
    }

    const cancelHandler = () => {
        setOpenModalMenu(false);
        setSearchInput('');
    }

    const openMenu = () => {
        setOpenModalMenu(true);
    }

    const returnFilteredData = (searchValue) => {
        if (searchValue !== '') {
            return metricsList.filter((item) => {
                return Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase())
            });
        } else {
            return metricsList;
        }
    }

    return (
        <div className="container-menu">
            <div className="btn-group">
                {metricsList.length > 0 && <ButtonList list={listForButtons} handleItemClick={handleMetricClick} color={chosenIndex} />}
                <button onClick={clickAddMetric} title="addMetric">+</button>
            </div>
            <div className="container-content">
                {!newMetric && !chosenIndex ? 'Selecteer een metric of maak een nieuwe aan.' : ''}
                {chosenIndex && <MetricsContent metricsContent={metricContent} fetchAllMetrics={fetchAllMetrics} />}
                {newMetric ? <MetricsAdd fetchAllMetrics={fetchAllMetrics} /> : ''}
            </div>
            <button className="button-menu" onClick={openMenu} title="menu"><SearchIcon /></button>
            <div>
                <button className="button-add" onClick={clickAddMetric}>+</button>
            </div>
            <div>
                {openModalMenu && <ModalMenu modalQuestion="Maak een keuze uit het menu" cancelHandler={cancelHandler} modalCancelText="Annuleren" handleClick={handleMetricClick} listForButtons={returnFilteredData(searchInput)} listForResults={(e) => setSearchInput(e.target.value)} />}
                {openModalMenu && <Backdrop cancelModal={cancelHandler} />}
            </div>
        </div>
    );
}

export default MetricsMenu;