import { useState, useEffect } from "react";
import Backdrop from "../modal/Backdrop";
import checkModal from "../../Functions/checkModal";
import RoundForm from "./RoundForm";

const RoundContent = ({ roundContent, fetchAllRounds }) => {
    const [scenarioDropdown, setScenarioDropdown] = useState([]);
    const [timerDropdown, setTimerDropdown] = useState([]);
    const [canvasDropdown, setCanvasDropdown] = useState([]);
    const [updatedRound, setUpdatedRound] = useState(null);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [modalSaveOpen, setModalSaveOpen] = useState(false);

    useEffect(() => {
        fetchAllScenarios();
        fetchAllTimers();
        fetchAllCanvas();
        setUpdatedRound(roundContent);
    }, [roundContent]);

    const fetchAllScenarios = async () => {
        const res = await fetch('http://localhost:8080/api/scenario');
        const data = await res.json();
        setScenarioDropdown(data);
    }

    const fetchAllTimers = async () => {
        const res = await fetch('http://localhost:8080/api/timer');
        const data = await res.json();
        setTimerDropdown(data);
    }

    const fetchAllCanvas = async () => {
        const res = await fetch('http://localhost:8080/api/canvas');
        const data = await res.json();
        setCanvasDropdown(data);
    }

    const deleteRound = async () => {
        await fetch(`http://localhost:8080/api/round/delete/${roundContent.id}`, {
            method: 'DELETE'
        });
        fetchAllRounds();
    }

    const putUpdatedRound = async (e) => {
        e.preventDefault();
        setModalSaveOpen(true);
        if (modalSaveOpen) {
            await putRequest();
        }
    }

    const putRequest = async () => {
        await fetch(`http://localhost:8080/api/round/update/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedRound)
        });
        fetchAllRounds();
    }

    const handleCanvasDropdown = (e, index) => {
        const updatedCanvases = updatedRound.canvases;
        canvasDropdown.forEach(canvas => {
            if (index === 1) {
                if (e.target.value === canvas.name) {
                    updatedCanvases[0] = canvas;
                    setUpdatedRound({ ...updatedRound, canvases: updatedCanvases });
                }
            } else if (index === 2) {
                if (e.target.value === canvas.name) {
                    updatedCanvases[1] = canvas;
                    setUpdatedRound({ ...updatedRound, canvases: updatedCanvases });
                }
            } else if (index === 3) {
                if (e.target.value === canvas.name) {
                    updatedCanvases[2] = canvas;
                    setUpdatedRound({ ...updatedRound, canvases: updatedCanvases });
                }
            } else if (index === 4) {
                if (e.target.value === canvas.name) {
                    updatedCanvases[3] = canvas;
                    setUpdatedRound({ ...updatedRound, canvases: updatedCanvases });
                }
            } else if (index === 5) {
                if (e.target.value === canvas.name) {
                    updatedCanvases[4] = canvas;
                    setUpdatedRound({ ...updatedRound, canvases: updatedCanvases });
                }
            }
        });
    }

    const handleScenarioDropdown = (e) => {
        scenarioDropdown.forEach(scenario => {
            if (e.target.value === scenario.title) {
                setUpdatedRound({ ...updatedRound, scenario: scenario });
            }
        });
    }

    const handleTimerDropdown = (e) => {
        timerDropdown.forEach(timer => {
            if (e.target.value === timer.name) {
                setUpdatedRound({ ...updatedRound, timer: timer });
            }
        });
    }

    const exportJSON = async () => {
        closeModalHandler();
        let a = document.createElement('a');
        a.href = `http://localhost:8080/api/download/round/${updatedRound.id}`;
        a.setAttribute('download', 'file');
        a.click();
        a.remove();
        return false;
    }

    const closeModalHandler = () => {
        setModalIsOpen(false);
        setModalSaveOpen(false);
    }

    const openModalHandler = () => {
        setModalIsOpen(true);
    }

    return (
        <div className="div-content">
            {updatedRound !== null && (
                <RoundForm round={updatedRound} setRound={setUpdatedRound}
                    scenarioDropdown={scenarioDropdown} timerDropdown={timerDropdown} canvasDropdown={canvasDropdown}
                    handleCanvasDropdown={handleCanvasDropdown} handleScenarioDropdown={handleScenarioDropdown}
                    handleTimerDropdown={handleTimerDropdown} openModalHandler={openModalHandler} />
            )}
            <div className="div-save-button">
                <input className="button-save" onClick={putUpdatedRound} type="submit" value="Opslaan" />
                <button className="button-delete" onClick={openModalHandler}>Verwijder ronde</button>
            </div>
            {(modalIsOpen || modalSaveOpen) && <Backdrop cancelModal={closeModalHandler} />}
            {checkModal(modalIsOpen, modalSaveOpen, closeModalHandler, deleteRound, putUpdatedRound)}
            <div style={{marginTop: "20px"}} className="div-save-button"><button className="button-export" onClick={exportJSON}>JSON Export Round</button></div>
        </div>
    );
};

export default RoundContent;
