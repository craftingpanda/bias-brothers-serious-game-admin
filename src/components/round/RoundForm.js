
const RoundForm = ({ postOrPut, round, setRound, scenarioDropdown, timerDropdown, canvasDropdown,
    handleCanvasDropdown, handleScenarioDropdown, handleTimerDropdown }) => {
    return (
        <form onSubmit={postOrPut} className="roundContent-form">
            <div className="div-round">
            <label>Rondetitel</label>
                <label>Rondenummer</label>
                <textarea onChange={e => setRound({...round, title: e.target.value})} value={round.title}/>
            <textarea onChange={e => setRound({...round, roundNumber: e.target.value})} value={round.roundNumber}/>
            </div>
            <label>Scenario</label>
            <select onChange={e => handleScenarioDropdown(e)}>
                <option defaultValue="Kies een scenario">Kies een scenario</option>
                {scenarioDropdown.map(scenario => <option key={scenario.id} value={scenario.title}>{scenario.title}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={round.scenario.title} />
            <label>Timer</label>
            <select onChange={e => handleTimerDropdown(e)}>
                <option defaultValue="Kies een timer">Kies een timer</option>
                {timerDropdown.map(timer => <option key={timer.id} value={timer.name}>{timer.name}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={round.timer.name} />
            <label>Canvas 1</label>
            <select onChange={e => handleCanvasDropdown(e, 1)}>
                <option defaultValue="Kies een canvas">Kies een canvas</option>
                {canvasDropdown.map(canvas => <option key={canvas.id} value={canvas.name}>{canvas.name}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={round.canvases[0].name}/>
            <label>Canvas 2</label>
            <select onChange={e => handleCanvasDropdown(e, 2)}>
                <option defaultValue="Kies een canvas">Kies een canvas</option>
                {canvasDropdown.map(canvas => <option key={canvas.id} value={canvas.name}>{canvas.name}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={round.canvases[1].name}/>
            <label>Canvas 3</label>
            <select onChange={e => handleCanvasDropdown(e, 3)}>
                <option defaultValue="Kies een canvas">Kies een canvas</option>
                {canvasDropdown.map(canvas => <option key={canvas.id} value={canvas.name}>{canvas.name}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={round.canvases[2].name}/>
            <label>Canvas 4</label>
            <select onChange={e => handleCanvasDropdown(e, 4)}>
                <option defaultValue="Kies een canvas">Kies een canvas</option>
                {canvasDropdown.map(canvas => <option key={canvas.id} value={canvas.name}>{canvas.name}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={round.canvases[3].name}/>
            <label>Canvas 5</label>
            <select onChange={e => handleCanvasDropdown(e, 5)}>
                <option defaultValue="Kies een canvas">Kies een canvas</option>
                {canvasDropdown.map(canvas => <option key={canvas.id} value={canvas.name}>{canvas.name}</option>)}
            </select>
            <textarea className="greyed-out" readOnly value={round.canvases[4].name}/>
        </form>
    );
};

export default RoundForm;
