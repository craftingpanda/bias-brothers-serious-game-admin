import { useState, useEffect, useCallback } from "react";
import ButtonList from "../ButtonList";
import ModalMenu from "../modal/ModalMenu";
import Backdrop from "../modal/Backdrop";
import SearchIcon from '@mui/icons-material/Search';
import MeasureAdd from './MeasureAdd';
import MeasureContent from './MeasureContent';

const MeasureMenu = () => {
    const [measureList, setMeasureList] = useState([]);
    const [listForButtons, setListForButtons] = useState([]);
    const [chosenIndex, setChosenIndex] = useState(0);
    const [newMeasure, addMeasure] = useState(false);
    const [measureContent, setMeasureContent] = useState([]);
    const [openModalMenu, setOpenModalMenu] = useState(false);
    const [searchInput, setSearchInput] = useState('');

    const fetchAllMeasures = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/measure');
        const data = await res.json();
        setMeasureList(data);
        setButtons(data);
        setChosenIndex(false);
        addMeasure(false);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            fetchAllMeasures();
        }
        fetchData();
    }, [fetchAllMeasures]);

    const setButtons = (data) => {
        const buttonList = [];
        data.forEach((measure) => {
            const button = { id: measure.id, name: measure.answer }
            buttonList.push(button);
        });
        setListForButtons(buttonList);
    }

    const handleMeasureClick = (clickedMeasureId) => {
        measureList.forEach(measure => {
            if (measure.id === clickedMeasureId) {
                addMeasure(false);
                setMeasureContent(measure);
                setChosenIndex(clickedMeasureId);
                setOpenModalMenu(false);
                setButtons(measureList);
            }
        });
    }

    const cancelHandler = () => {
        setOpenModalMenu(false);
        setButtons(measureList);
    }

    const openMenu = () => {
        setOpenModalMenu(true);
    }

    const clickAddMeasure = () => {
        setChosenIndex(false);
        addMeasure(true)
    }

    const returnFilteredData = (searchValue) => {
        setSearchInput(searchValue);
        if (searchInput !== '') {
            const filteredData = measureList.filter((item) => {
                return Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase())
            })
            setButtons(filteredData);
        } else {
            setButtons(measureList);
        }
    }

    return (
        <div className="container-menu">
            <div className="btn-group-long">
                {measureList.length > 0 && <ButtonList list={listForButtons} handleItemClick={handleMeasureClick} color={chosenIndex} />}
                <button onClick={clickAddMeasure} title="addMeasure">+</button>
            </div>
            <div className="container-content">
                {!newMeasure && !chosenIndex ? 'Selecteer een maatregel of maak een nieuwe aan.' : ''}
                {chosenIndex && <MeasureContent measureContent={measureContent} fetchAllMeasures={fetchAllMeasures} />}
                {newMeasure ? <MeasureAdd fetchAllMeasures={fetchAllMeasures} /> : ''}
            </div>
            <button className="button-menu" onClick={openMenu} title="menu"><SearchIcon /></button>
            <div>
                <button className="button-add" onClick={clickAddMeasure}>+</button>
            </div>
            <div>
                {openModalMenu && <ModalMenu modalQuestion="Maak een keuze uit het menu" cancelHandler={cancelHandler} modalCancelText="Annuleren" handleClick={handleMeasureClick} listForButtons={listForButtons} listForResults={(e) => returnFilteredData(e.target.value)} />}
                {openModalMenu && <Backdrop cancelModal={cancelHandler} />}
            </div>
        </div>
    );
}

export default MeasureMenu;
