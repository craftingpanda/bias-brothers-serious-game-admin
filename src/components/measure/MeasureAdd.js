import { useState } from "react";
import Modal from "../modal/Modal";
import Backdrop from "../modal/Backdrop";

const MeasureAdd = ({ fetchAllMeasures }) => {
    const [textMeasureAnswer, setTextMeasureAnswer] = useState('');
    const [alertModal, setAlertModal] = useState(null);
    const [emptyFieldModal, setEmptyFieldModal] = useState(null);

    const postMeasure = async (e) => {
        e.preventDefault();
        if (textMeasureAnswer === '') {
            setEmptyFieldModal(true);
            return;
        }
        const measure = {
            answer: textMeasureAnswer
        };
        setAlertModal(true);
        if (alertModal) {
            await postRequest(measure);
        }

    }

    const postRequest = async (measure) => {
        await fetch('http://localhost:8080/api/measure/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(measure)
        });
        fetchAllMeasures();
    }

    const closeModalHandler = () => {
        setAlertModal(false);
        setEmptyFieldModal(false);
    }

    return (
        <div className="div-content">
            <form className="measureContent-form" onSubmit={postMeasure}>
                <div>
                    <label>Maatregel antwoord</label>
                </div>
                <div>
                    <textarea value={textMeasureAnswer} onChange={e => setTextMeasureAnswer(e.target.value)} />
                </div>
                <div className="div-save-button">
                    <input className="button-save" type="submit" value="Opslaan" />
                </div>
                <div>
                    {/* Opens the modal for the save button. */}
                    {alertModal && <Modal cancelHandler={closeModalHandler} confirmHandler={postMeasure} modalQuestion='Weet je zeker dat je een nieuwe maatregel wilt toevoegen?' modalCancelText='Annuleren' modalConfirmText='Opslaan' />}
                    {alertModal && <Backdrop cancelModal={closeModalHandler} />}
                    {/* Opens the modal when there are empty fields */}
                    {emptyFieldModal && <Modal cancelHandler={closeModalHandler} confirmHandler={closeModalHandler} modalQuestion='Vul alle velden in voordat je gaat opslaan!' modalCancelText='Sluiten' modalConfirmText='Velden invullen' />}
                    {emptyFieldModal && <Backdrop cancelModal={closeModalHandler} />}
                </div>
            </form>
        </div>
    );
}

export default MeasureAdd;
