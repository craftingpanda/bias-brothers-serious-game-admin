import Navbar from './Navbar';
import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';

test('On initial render, the "Spel" button to be enabled', () => {
    render(<Navbar />);

    expect(screen.getByRole('button', { name: /Spel/i })).toBeEnabled();
});
