import logo from '../../images/logo-zonder-tekst.png';
import { Link } from 'react-router-dom';

const Navbar = ({ setPageIndex, pageIndex }) => {

    return (
        <div className="container-navbar">
            <div className="div-logo">
                <img className="img-logo" src={logo} alt="logo" />
            </div>
            <div className="btn-group-navbar">
                <Link to="/game">
                    <button onClick={() => setPageIndex(1)} id={pageIndex === 1 ? "button-clicked" : undefined}>Game</button>
                </Link>
                <Link to="/ronde">
                    <button onClick={() => setPageIndex(2)} id={pageIndex === 2 ? "button-clicked" : undefined}>Ronde</button>
                </Link>
                <Link to="/canvas">
                    <button onClick={() => setPageIndex(3)} id={pageIndex === 3 ? "button-clicked" : undefined}>Canvas</button>
                </Link>
                <Link to="/nieuwsberichten">
                    <button onClick={() => setPageIndex(4)} id={pageIndex === 4 ? "button-clicked" : undefined}>Nieuwsberichten</button>
                </Link>
                <Link to="/scenario">
                    <button onClick={() => setPageIndex(5)} id={pageIndex === 5 ? "button-clicked" : undefined}>Scenario's</button>
                </Link>
                <Link to="/bias">
                    <button onClick={() => setPageIndex(6)} id={pageIndex === 6 ? "button-clicked" : undefined}>Biases</button>
                </Link>
                <Link to="/maatregelen">
                    <button onClick={() => setPageIndex(7)} id={pageIndex === 7 ? "button-clicked" : undefined}>Maatregelen</button>
                </Link>
                <Link to="/metrieken">
                    <button onClick={() => setPageIndex(8)} id={pageIndex === 8 ? "button-clicked" : undefined}>Metrieken</button>
                </Link>
                <Link to="/timer">
                    <button onClick={() => setPageIndex(9)} id={pageIndex === 9 ? "button-clicked" : undefined}>Timer</button>
                </Link>

            </div>
        </div>
    );
}

export default Navbar;
