import './styles/styleStandard.css';
import './styles/styleButtons.css';
import './styles/styleContent.css';
import Navbar from './components/navBar/Navbar';
import GameMenu from './components/game/GameMenu';
import BiasMenu from './components/bias/BiasMenu';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import { useState } from 'react';
import ScenarioMenu from './components/scenario/ScenarioMenu';
import NewsArticleMenu from './components/newsarticle/NewsArticleMenu';
import MetricsMenu from "./components/metrics/MetricsMenu";
import MeasureMenu from "./components/measure/MeasureMenu";
import CanvasMenu from './components/canvas/CanvasMenu';
import TimerMenu from './components/timer/TimerMenu';
import Home from './components/home/Home';
import React from 'react';
import RoundMenu from './components/round/RoundMenu'

function App() {
    const [pageIndex, setPageIndex] = useState(0);

    return (
        <Router>
            <div className="container-app">
                <Navbar setPageIndex={setPageIndex} pageIndex={pageIndex} />
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/game" component={GameMenu} />
                        <Route exact path="/ronde" component={RoundMenu} />
                        <Route exact path="/canvas" component={CanvasMenu} />
                        <Route exact path="/nieuwsberichten" component={NewsArticleMenu} />
                        <Route exact path="/bias" component={BiasMenu} />
                        <Route exact path="/scenario" component={ScenarioMenu} />
                        <Route exact path="/maatregelen" component={MeasureMenu} />
                        <Route exact path="/metrieken" component={MetricsMenu} />
                        <Route exact path="/timer" component={TimerMenu} />
                    </Switch>
            </div>
        </Router>
    );
}

export default App;


