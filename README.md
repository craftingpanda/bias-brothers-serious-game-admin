# Bias Brothers - Serious Game Admin
This project is made for the Bias Brothers so they could
edit the content for the digital game of their serious game.
We made this project in 4 sprints of 2 weeks.

# Information
npm install
This project makes use of React
To start the program use npm start

# All other projects listed
Underneath you can find the name and location of the projects, see its own README to know which branches work together.
-----------------------------------------------------------------------------------------------------------------------
The front-end of the serious game application: Bias Brothers - Serious Game <br>
You can find at: https://gitlab.com/craftingpanda/bias-brothers-serious-game

The back-end in javaSpark for the serious game: Bias Brothers - Serious Game back-end <br>
You can find at: https://gitlab.com/craftingpanda/bias-brothers-serious-game-back-end

The front-end in React of the serious game admin: Bias Brothers - Serious Game Admin <br>
You can find at: https://gitlab.com/craftingpanda/bias-brothers-serious-game-admin

The new back-end of the serious game application & admin side : Bias Brothers - Serious Game Admin back-end <br>
You can find at: https://gitlab.com/craftingpanda/bias-brothers-serious-game-admin-back-end

# Version 1 - Team project
Works with the application Bias Brothers - Serious Game Admin back-end @version 1 <br>
This version works with branch version 1 see link above for the repository.

# Version 2 - Finishing all 4 projects on my own
Works with the application Bias Brothers - Serious Game @version 3 and Serious Game Admin back-end @version 2<br>
This project was to make it so the old back-end was not needed anymore.

But to give more options to use, I made it suitable on the new structure of the json file.
This front-end provides for the serious game version 3 see link above for the repository.

# Full list of all projects and branches that work together
# Bias Brother - Serious Game
Serious Game Version 1 - Serious Game back-end Version 1 <br>
Serious Game Version 2 - Serious Game back-end Version 2 <br>
Serious Game Version 3 - Serious Game Admin Version 2 <br>

# Bias Brother - Serious Game back-end
Serious Game back-end Version 1 - Serious Game Version 1 <br>
Serious Game back-end Version 2 - Serious Game Version 2 <br>

# Bias Brother - Serious Game Admin
Serious Game Admin Version 1 - Serious Game Admin back-end Version 1 <br>
Serious Game Admin Version 2 - Serious Game Version 3  <br>
Serious Game Admin Version 2 - Serious Game Admin back-end Version 2 <br>

# Bias Brother - Serious Game Admin back-end
Serious Game Admin back-end Version 1 - Serious Game Admin Version 1 <br>
Serious Game Admin back-end Version 2 - Serious Game Version 3 <br>
Serious Game Admin back-end Version 2 - Serious Game Admin Version 2 <br>